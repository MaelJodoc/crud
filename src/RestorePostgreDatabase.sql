DROP TABLE IF EXISTS book;
CREATE TABLE book
(
  id          SERIAL PRIMARY KEY NOT NULL,
  title       VARCHAR(100) DEFAULT '' NOT NULL,
  description VARCHAR(255) DEFAULT '' NOT NULL,
  author      VARCHAR(100) DEFAULT '' NOT NULL,
  isbn        VARCHAR(20)  DEFAULT '' NOT NULL,
  print_year   INT                NOT NULL,
  read_already BOOLEAN      DEFAULT FALSE NOT NULL

);

INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('GIT для профессионального программиста',
        'Эта книга представляет собой обновленное руководство по использованию Git в современных условиях.',
        'Чакон',
        '978-5-496-01763-3',
        2016,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Изучаем Java EE 7',
        'Данная книга представляет собой логичное пошаговое руководство, в котором подробно описаны многие спецификации и эталонные реализации Java EE 7.',
        'Гонсалвес',
        '978-5-496-00942-3',
        2014,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Архитектура корпоративных программных приложений',
        'Книга предназначена для программистов, проектировщиков и архитекторов, которые занимаются созданием корпоративных приложений и стремятся повысить качество принимаемых стратегических решений.',
        'Фаулер',
        '5-8459-0579-6',
        2006,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Изучаем Java',
        '"Изучаем Java" - это не просто книга. Она не только научит Вас теории языка Java и объектно-ориеитированиого программирования, она сделает Вас программистом.',
        'Сьерра',
        'unknown',
        2012,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Изучаем SQL',
        'Эта книга посвящена языку запросов SQL и управлению базами данных. Материал излагается, начиная с описания базовых запросов и заканчивая сложными манипуляциями с помощью объединений, подзапросов и транзакций.',
        'Бейли',
        '978-5-459-00421-2',
        2012,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Приемы объектно-ориентированного проектирования. Паттерны проектирования',
        'В предлагаемой книге описываются простые и изящные решения типичных задач, возникающих в объектно-ориентированном проектировании.',
        'Гамма',
        '5-272-00355-1',
        2001,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Scrum и XP: заметки с передовой',
        'Основная ценность книги Хенрика состоит в том, что если вы будете следовать его советам, то у вас будет и product backlog, и оценки для product backlog''а, и burndown-диаграмма.',
        'Книберг',
        'unknown',
        2015,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Spring 4 для профессионалов',
        'Руководство по Spring Framework, соответствующее отраслевым стандартам',
        'Шефер',
        '978-5-8459-1992-2',
        2015,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Грокаем алгоритмы',
        'Откройте великолепно иллюстрированную книгу, и вы сразу поймете, что алгоритмы - это просто. А грокать алгоритмы - это веселое и увлекательное занятие.',
        'Бхаргава',
        '978-5-496-02541-6',
        2017,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Философия JAVA',
        'Книга считается одним из лучших пособий для программиста',
        'Эккель',
        '978-5-496-01127-3',
        2015,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Искусство программирования',
        'Эта книга - великолепное пособие по составлению и анализу компьютерных алгоритмов',
        'Кнут',
        'unknown',
        1990,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Структуры данных и алгоритмы в Java',
        'Первая часть книги представляет собой введение в алгоритмизацию и структуры данных, а также содержит изложение основ объектно-ориентированного программирования.',
        'Лафоре',
        '978-5-496-00740-5',
        2011,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Легкий способ бросить программировать и начать спать',
        'Для отчаявшихся',
        'Карр',
        'unknown',
        2017,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Дискретная математика для программистов',
        'Основополагающее введение в дискретную математику, без знания которой невозможно успешно заниматься информатикой и программированием',
        'Хаггарти',
        '978-5-94836-303-5',
        2012,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Комбинаторика для программистов',
        'В настоящей книге представленны некоторые разделы комбинаторики, причем особое внимание уделено конструктивному алгоритмическому подходу',
        'Липский',
        'unknown',
        1988,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Java эффективное программирование',
        'Эта книга посвящена общепринятыым и эффективным приемам работы с языком Java',
        'Блох',
        '978-0-321-3568-0',
        2014,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Java. Библиотека профессионала',
        'Цель книги - помочь вам понять язык Java и его библиотеки в полной мере, а не создать иллюзию такого понимания',
        'Хорстманн',
        '978-5-8459-1886-4',
        2014,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('EJB 3 в действии',
        'Фреймворк EJB 3 предоставляет стандартный способ оформления прикладной логики в виде управляемых модулей, которые выполняются на стороне сервера, упрощая тем самым создание, сопровождение и расширение приложений.',
        'Панда',
        '978-5-97060-135-8',
        2015,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Java Concurrency in Practice',
        'Threads are a fundamental part of the Java platform. As multicore processors become the norm, using concurrency effectively becomes essential for building high-performance applications.',
        'Goetz',
        '978-0-321-34960-6',
        2006,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Реактивное программирование с использованием RxJava',
        'Благодаря этой книге программист на Java узнает о реактивном подходе к задачам и научится создавать программы, вобравшие в себя лучшие черты этой новой и весьма перспективной парадигмы.',
        'Нуркевич',
        '978-5-97060-496-0',
        2017,
        FALSE);
INSERT INTO book (title, description, author, isbn, print_year, read_already)
VALUES ('Алгоритмы на Java',
        'Книга Седжвика и Уэйна «Алгоритмы на Java» является классическим справочным руководством в котором содержится необходимый объем знаний для программиста в области алгоритмов.',
        'Седжвик',
        '978-5-8459-2049-2',
        2016,
        FALSE);




