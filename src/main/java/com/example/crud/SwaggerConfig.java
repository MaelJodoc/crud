package com.example.crud;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Created by Смена on 03.02.2018.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    //http://localhost:8080/v2/api-docs
    //http://localhost:8080/swagger-ui.html
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("com.example.crud.controller"))
                .paths(regex("/books.*"))
                .build();
    }
}
