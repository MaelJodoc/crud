package com.example.crud.repository;

import com.example.crud.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by Ebozavreg on 16.12.2017.
 */
@Repository
public interface BooksRepository extends JpaRepository<Book, Integer> {

}
