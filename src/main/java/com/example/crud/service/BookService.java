package com.example.crud.service;

import com.example.crud.model.Book;
import org.springframework.dao.EmptyResultDataAccessException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Created by Ebozavreg on 31.01.2018.
 */
public interface BookService {
    Book addBook(Book book);

    Book getBookById(int id) throws EntityNotFoundException;

    List<Book> getBooks(int pageNumber, int pageSize);

    List<Book> getAllBooks();

    int getBookNumber();

    void deleteBook(int id) throws EmptyResultDataAccessException;

    Book updateBook(Book updatedBook);

    void markBookRead(int id);

}
