package com.example.crud.service;

import com.example.crud.model.Book;
import com.example.crud.repository.BooksRepository;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Ebozavreg on 31.01.2018.
 */
@Service
public class BookServiceImpl implements BookService {
    private static final int MAX_PAGE_SIZE = 100;
    private static final int DEFAULT_PAGE_SIZE = 10;
    @Autowired
    private BooksRepository booksRepository;
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Book addBook(Book book) {
        return booksRepository.save(book);
    }

    @Override
    public Book getBookById(int id) throws EntityNotFoundException {
        Book book = booksRepository.findOne(id);
        if (book == null) throw new EntityNotFoundException();
        return book;
    }

    @Override
    @Transactional
    public List<Book> getBooks(int pageNumber, int pageSize) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery(Book.class);
        Root<Book> root = criteriaQuery.from(Book.class);
        CriteriaQuery<Book> select = criteriaQuery.select(root).orderBy(criteriaBuilder.asc(root.get("title")));
        TypedQuery<Book> typedQuery = entityManager.createQuery(select);
        if (pageSize > MAX_PAGE_SIZE) pageSize = MAX_PAGE_SIZE;
        if (pageSize < 1) pageSize = DEFAULT_PAGE_SIZE;
        typedQuery.setMaxResults(pageSize);
        typedQuery.setFirstResult((pageNumber - 1) * pageSize);
        List<Book> books = typedQuery.getResultList();
        entityManager.close();
        return books;
    }

    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAll();
    }

    @Override
    public int getBookNumber() {
        return (int) booksRepository.count();
    }

    @Override
    public void deleteBook(int id) throws EmptyResultDataAccessException {
        booksRepository.delete(id);
    }

    @Override
    @Transactional
    public Book updateBook(Book updatedBook) throws EntityNotFoundException {
        Book book = getBookById(updatedBook.getId());
        updatedBook.setAuthor(book.getAuthor());
        updatedBook.setReadAlready(false);
        booksRepository.save(updatedBook);
        return updatedBook;
    }

    @Override
    @Transactional
    public void markBookRead(int id) throws EntityNotFoundException {
        Book book = getBookById(id);
        book.setReadAlready(true);
        booksRepository.save(book);
    }
}
