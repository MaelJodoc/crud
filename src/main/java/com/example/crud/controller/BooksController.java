package com.example.crud.controller;

import com.example.crud.model.Book;
import com.example.crud.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Created by Ebozavreg on 16.12.2017.
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/books")
public class BooksController {
    @Autowired
    private BookService bookService;

    @GetMapping("/getAll")
    public ResponseEntity<List<Book>> getAllBooks() {
        List<Book> books = bookService.getAllBooks();
        return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Book> getBookById(@PathVariable int id) {
        try {
            Book book = bookService.getBookById(id);
            return new ResponseEntity<Book>(book, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/get")
    public ResponseEntity<List<Book>> getPage(@RequestParam("page") int page,
                                              @RequestParam(name = "size", required = false) Integer size) {
        if (size == null) size = 0;
        List<Book> books = bookService.getBooks(page, size);
        return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
    }


    @GetMapping("/count")
    public int getBookNumber() {
        return bookService.getBookNumber();
    }

    @PostMapping("/add")
    public ResponseEntity<Book> addBook(@RequestBody Book book) {
        if (!isValid(book)) return new ResponseEntity<Book>(HttpStatus.BAD_REQUEST);
        book.setId(0);
        Book b = bookService.addBook(book);
        return new ResponseEntity<Book>(b, HttpStatus.OK);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Void> deleteBook(@RequestBody int id) {
        try {
            bookService.deleteBook(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<Book> updateBook(@RequestBody Book updatedBook) {
        if (!isValid(updatedBook)) return new ResponseEntity<Book>(HttpStatus.BAD_REQUEST);
        try {
            Book book = bookService.updateBook(updatedBook);
            return new ResponseEntity<Book>(book, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/setRead")
    public ResponseEntity<Void> markBookAsRead(@RequestBody int id) {
        try {
            bookService.markBookRead(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }

    private boolean isValid(Book book) {
        return (book.getAuthor().length() <= 100 &&
                book.getDescription().length() <= 255 &&
                book.getTitle().length() <= 100 &&
                book.getIsbn().length() <= 20);
    }
}
